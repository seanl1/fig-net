# fig-net

A federation of communities built on the technology of Usenet.

# Background

We, the creators of FIGnet, believe that Usenet's decline came not because of the influx of people into the Internet and the ensuing decline of "netiquette", but because Usenet access was primarily provided by Internet Service Providers (ISPs), who were poor stewards. We believe that a Usenet-like network of non-commercial, community focused servers can be sustainable and scalable with appropriate governance.

# Similar efforts

The closest thing to what we're trying to do is [the Fediverse](https://en.wikipedia.org/wiki/Fediverse). The Fediverse is oriented around accounts and servers, with hashtags as a very weak mechanism for organizing posts topically, while FIGnet is organized primarily around topical/community-oriented groups. Local groups on servers will still promote server-oriented communities as well as providing a place to discuss server administration, but we do not envision servers as the primary mechanism around which communities will form.

# Standards

Like the Fediverse, FIGnet will be defined by a set of standards and protocols. Unlike with the Fediverse, it will not be sufficient to simply set up the software to call yourself part of FIGnet. While the software is mature and open source (it is the same software that Usenet runs on) and we cannot people from running it, FIGnet servers will not be allowed to "leak" FIGnet groups to non-FIGnet servers, lest they lose their peering connections to the rest of FIGnet. FIGnet servers and groups will also be required to have enforced codes of conduct. While we value freedom of speech, FIGnet is intended to be a place where communities can thrive, not a place for abuse. As such, intolerance will not be tolerated. The full "meta-code of conduct" will be published later.

# Changes

FIGnet will evolve over time. This document lives in a Git repository specifically so that it can be changed. FIGnet will be governed by its participants, though it's not yet clear exactly what this will look like.

# Open questions

- Should commercial servers be allowed?
- Should commercial groups be allowed?
- To what extent should the carrying of unmoderated non-FIGnet groups be tolerated on FIGnet servers?
- How should governance work? 